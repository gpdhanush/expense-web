import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpResponse} from "../_models/HttpResponse";
import {apiUrl} from "../_config/UrlConfig";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategoryList(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'getCategory.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  saveCategory(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'saveCategory.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  deleteCategory(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'deleteCategory.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
}
