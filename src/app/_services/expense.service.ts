import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpResponse} from "../_models/HttpResponse";
import {apiUrl} from "../_config/UrlConfig";
import {map, Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  constructor(private http: HttpClient) { }

  getExpenseMaster(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'getExpenses.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  typeBasedCategory(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'getExpenseBasedCategory.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  saveExpenseMaster(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'saveExpense.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  expenseUpload(formData: any) {
    return this.http.post(apiUrl.BASE_API_URL + 'expenseUpload.php', formData, {
      reportProgress: true,
      observe: 'events'
    });
  }
}
