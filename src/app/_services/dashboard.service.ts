import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpResponse} from "../_models/HttpResponse";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getHash(params: any): Observable<any> {
    const headers = { encoding: 'UTF-8', 'Content-Type': 'application/json' }
    return this.http.post<any>('http://106.51.1.170:5100/API/RequestData', JSON.stringify(params), {'headers':headers});
  }
}
