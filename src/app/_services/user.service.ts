import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {HttpResponse} from "../_models/HttpResponse";
import {apiUrl} from "../_config/UrlConfig";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(loginDetails: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'login.php', JSON.stringify(loginDetails))
      .pipe(map((user: any) => {
      return user;
    }));
  }
}
