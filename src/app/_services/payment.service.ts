import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpResponse} from "../_models/HttpResponse";
import {apiUrl} from "../_config/UrlConfig";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) { }

  getPaymentModes(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'getPaymentMode.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  savePayments(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'savePayment.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
  deletePayments(params: any): Observable<HttpResponse> {
    return this.http.post<HttpResponse>(apiUrl.BASE_API_URL + 'deletePayments.php', JSON.stringify(params))
      .pipe(map((user: any) => {
        return user;
      }));
  }
}
