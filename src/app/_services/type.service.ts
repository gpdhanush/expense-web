import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpResponse} from "../_models/HttpResponse";
import {apiUrl} from "../_config/UrlConfig";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  constructor(private http: HttpClient) { }

  getTypes(): Observable<HttpResponse> {
    return this.http.get<HttpResponse>(apiUrl.BASE_API_URL + 'getTypes.php')
      .pipe(map((user: any) => {
        return user;
      }));
  }
}
