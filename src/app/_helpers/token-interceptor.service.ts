import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(<string>localStorage.getItem('currentUser'));
    const reqURL = request.url;
    if (reqURL.toLowerCase().includes('login'.toLowerCase())) {
      request = request.clone({
        setHeaders: {
          'Content-Type': `application/json`,
        }
      });
      // token not needed
    } else {
      // if (currentUser) {
      //   request = request.clone({
      //     setHeaders: {
      //       token: currentUser.token,
      //     }
      //   });
      // }
    }
    return next.handle(request);
  }
}
