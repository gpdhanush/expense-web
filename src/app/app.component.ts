import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {setTheme} from "ngx-bootstrap/utils";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'expense-web';

  constructor(private spinner: NgxSpinnerService) {
    setTheme('bs3');
  }
  ngOnInit(): void {
  }
}
