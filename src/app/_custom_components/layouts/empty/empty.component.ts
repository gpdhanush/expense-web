import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit(): void {
    document.body.classList.add('hold-transition', 'login-page');
  }

  ngOnDestroy(): void {
    document.body.classList.remove('hold-transition', 'login-page');
  }

}
