export class User {
  id: string | undefined;
  fullName: string | undefined;
  userName: string | undefined;
  email: string | undefined;
  mobile: string | undefined;
  profileImage: string | undefined;
  createDt: string | undefined;
  updateDt: string | undefined;
  activeStatus: string | undefined;
}
