import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmptyComponent} from "./_custom_components/layouts/empty/empty.component";
import {AuthGuard} from "./_guards/auth.guard";
import {FullComponent} from "./_custom_components/layouts/full/full.component";

const routes: Routes = [
  {
    path: '', component: EmptyComponent, children: [
      {
        path: '',
        redirectTo: 'user',
        pathMatch: 'full',
      },
      {
        path: 'user',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./_modules/user/user.module').then(m => m.UserModule)
      },
    ]
  },
  {
    path: '', component: FullComponent, children: [
      {
        path: '',
        redirectTo: 'admin',
        pathMatch: 'full',
      },
      {
        path: 'admin',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./_modules/admin/admin.module').then(m => m.AdminModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
