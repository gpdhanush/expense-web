import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {PaymentService} from "../../../_services/payment.service";
import {Router} from "@angular/router";
import {AlertifyService} from "../../../_services/alertify.service";
import {Subject} from "rxjs";
import {CategoryService} from "../../../_services/category.service";
import {TypeService} from "../../../_services/type.service";
declare var $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  categoryMaster: any;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  modalHeader = 'Add Master';
  form: FormGroup;
  currentUser: any;
  buttonLabel = 'Save Details';
  typeMaster: any;
  constructor(private spinner: NgxSpinnerService, private router: Router, private fb: FormBuilder,
              private alertService: AlertifyService, private categoryService: CategoryService,
              private typeService: TypeService) { }


  ngOnInit(): void {
    this.currentUser = JSON.parse(<string>localStorage.getItem('currentUser'));
    this.getCategoryList();
    this.getTypeMaster();
    this.initForm();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20, 25, 30],
      order: [],
      language: {searchPlaceholder: " Search"},
      columnDefs: [ {
        orderable: false,
        targets: [3]
      }]

    };
  }
  getTypeMaster(): any {
    this.spinner.show();
    this.typeService.getTypes().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.typeMaster = data.responseValue;
      }
    }, e => {
      this.spinner.hide();
      this.typeMaster = [];
    });
  }
  initForm(): any {
    this.form = this.fb.group({
      id: [''],
      type: [''],
      userId: [this.currentUser.id],
      name: ['', Validators.compose([Validators.required])]
    });
  }
  getCategoryList(): any {
    this.spinner.show();
    this.categoryService.getCategoryList({userId: this.currentUser.id}).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        this.categoryMaster = data.responseValue;
        this.dtTrigger.next('');
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.categoryMaster = [];
      this.dtTrigger.next('');
      this.alertService.error(error);
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  formSubmit(value: any) {
    this.spinner.show();
    this.categoryService.saveCategory(value).subscribe((data) => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.alertService.success(data.responseValue.message);
        $('#addModal').modal('hide');
        this.rerender();
        this.getCategoryList();
      }
    }, e => {
      this.spinner.hide();
      this.alertService.error(e.error.responseValue.message);
    });
  }
  editForm(list: any) {
    $('#addModal').modal('show');
    this.buttonLabel = 'Update Details';
    this.modalHeader = 'Update Master';
    this.form.patchValue({
      id: list.id,
      userId: list.userId,
      name: list.name,
      type: list.type,
    });
  }
  deleteList(list: any) {
    if (confirm('do you want to delete record ?')) {
      this.spinner.show();
      this.categoryService.deleteCategory(list).subscribe(data => {
        if (data.responseType === 'S') {
          this.spinner.hide();
          // alert(data.responseValue.message);
          this.alertService.success(data.responseValue.message);
          this.rerender();
          this.getCategoryList()
        }
      }, e => {
        this.spinner.hide();
        // alert(e.error.responseValue.message);
        this.alertService.error(e.error.responseValue.message);
      });
    }
  }
  addForm() {
    this.buttonLabel = 'Save Details';
    this.modalHeader = 'Add Master';
    $('#addModal').modal('show');
    this.form.reset();
    this.initForm();
  }
}
