import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExpenseComponent } from './expense/expense.component';
import {SharedModule} from "../../_helpers/shared.module";
import { PaymentsComponent } from './payments/payments.component';
import { CategoryComponent } from './category/category.component';
import { SettingsComponent } from './settings/settings.component';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  declarations: [
    DashboardComponent,
    ExpenseComponent,
    PaymentsComponent,
    CategoryComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    DataTablesModule,
    BsDatepickerModule.forRoot(),
  ]
})
export class AdminModule { }
