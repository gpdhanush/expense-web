import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {DashboardService} from "../../../_services/dashboard.service";
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  form: FormGroup;
  payment = false;
  paymentForm: any;
  constructor(private fb: FormBuilder, private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: '',
      Amount: '',
      key: 'MPY_Live_iX9XmgVAMDF6Qz1fCfSGE',
      MerchantTxnID: Math.floor(Math.random() * 999999999999).toString(),
      email: 'agp@gmail.com',
      Phone: '7845456609',
      surl: "http://localhost:52363/WebSite9/Success.aspx",
      furl: "http://localhost:52363/WebSite9/Failure.aspx",
      secret: "MPY_Live_cee8a3a0-0d9e-11ed-8042-d37ab4a4a076"
    });
    window.dispatchEvent( new Event( 'resize' ) );
  }

  submitForm(value: any) {
    console.log(value);
    this.payment = true;
    this.dashboardService.getHash(value).subscribe(data => {
      console.log(data);
      this.paymentForm = data;
    });
  }

  formSubmit() {
    $.document.getElementById('postform').submit();
  }
}
