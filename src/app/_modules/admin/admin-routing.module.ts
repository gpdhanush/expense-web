import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ExpenseComponent} from "./expense/expense.component";
import {PaymentsComponent} from "./payments/payments.component";
import {CategoryComponent} from "./category/category.component";
import {SettingsComponent} from "./settings/settings.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'expense', component: ExpenseComponent },
  { path: 'payments', component: PaymentsComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'settings', component: SettingsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
