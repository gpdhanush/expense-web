import {Component, OnInit, ViewChild} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {PaymentService} from "../../../_services/payment.service";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertifyService} from "../../../_services/alertify.service";
declare var $: any;
interface Payment {
  id: string | undefined;
  userId: string | undefined;
  name: string | undefined;
}

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
})
export class PaymentsComponent implements OnInit {
  paymentMaster: any;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  modalHeader = 'Add Payment Master';
  form: FormGroup;
  currentUser: any;
  buttonLabel = 'Save Details';
  constructor(private spinner: NgxSpinnerService, private paymentService: PaymentService,
              private router: Router, private fb: FormBuilder, private alertService: AlertifyService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(<string>localStorage.getItem('currentUser'));
    this.getPaymentModes();
    this.initForm();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20, 25, 30],
      order: [],
      language: {searchPlaceholder: " Search"},
      columnDefs: [ {
        orderable: false,
        targets: [2]
      }]

    };
  }
  initForm(): any {
    this.form = this.fb.group({
      id: [''],
      userId: [this.currentUser.id],
      name: ['', Validators.compose([Validators.required])]
    });
  }
  getPaymentModes(): any {
    this.spinner.show();
    this.paymentService.getPaymentModes({userId: this.currentUser.id}).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        this.paymentMaster = data.responseValue;
        this.dtTrigger.next('');
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.paymentMaster = [];
      this.dtTrigger.next('');
      this.alertService.error(error);
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  formSubmit(value: any) {
    this.spinner.show();
    this.paymentService.savePayments(value).subscribe((data) => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.alertService.success(data.responseValue.message);
        $('#addModal').modal('hide');
        this.rerender();
        this.getPaymentModes();
      }
    }, e => {
      this.spinner.hide();
      this.alertService.error(e.error.responseValue.message);
    });
  }
  editForm(list: Payment) {
    $('#addModal').modal('show');
    this.buttonLabel = 'Update Details';
    this.modalHeader = 'Update Payment Master';
    this.form.patchValue({
      id: list.id,
      userId: list.userId,
      name: list.name
    });
  }
  deleteList(list: any) {
    if (confirm('do you want to delete record ?')) {
      this.spinner.show();
      this.paymentService.deletePayments(list).subscribe(data => {
        if (data.responseType === 'S') {
          this.spinner.hide();
          // alert(data.responseValue.message);
          this.alertService.success(data.responseValue.message);
          this.rerender();
          this.getPaymentModes()
        }
      }, e => {
        this.spinner.hide();
        // alert(e.error.responseValue.message);
        this.alertService.error(e.error.responseValue.message);
      });
    }
  }
  addForm() {
    this.buttonLabel = 'Save Details';
    this.modalHeader = 'Add Payment Master';
    $('#addModal').modal('show');
    this.form.reset();
    this.initForm();
  }
}
