import {Component, OnInit, ViewChild} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {PaymentService} from "../../../_services/payment.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertifyService} from "../../../_services/alertify.service";
import {ExpenseService} from "../../../_services/expense.service";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs";
import {TypeService} from "../../../_services/type.service";
import {CategoryService} from "../../../_services/category.service";
import {HttpEventType} from "@angular/common/http";
import * as moment from "moment";
declare var $: any;

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {
  expenseLists: any;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  currentUser: any;
  form: FormGroup;
  buttonLabel: string;
  modalHeader: string;
  typeMaster: any;
  categoryMaster: any;
  paymentMaster: any;

  constructor(private spinner: NgxSpinnerService, private paymentService: PaymentService,
              private router: Router, private fb: FormBuilder, private alertService: AlertifyService,
              private expenseService: ExpenseService, private typeService: TypeService, private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(<string>localStorage.getItem('currentUser'));
    this.getExpenseLists();
    this.getCategoryList();
    this.getPaymentModes();
    this.getTypeMaster();
    this.initForm();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20, 25, 30],
      order: [],
      language: {searchPlaceholder: " Search"},
      columnDefs: [ {
        orderable: false,
        targets: [7]
      }]

    };
  }
  getPaymentModes(): any {
    this.spinner.show();
    this.paymentService.getPaymentModes({userId: this.currentUser.id}).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        this.paymentMaster = data.responseValue;
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.paymentMaster = [];
      this.alertService.error(error);
    });
  }
  getCategoryList(): any {
    this.spinner.show();
    this.categoryService.getCategoryList({userId: this.currentUser.id}).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        this.categoryMaster = data.responseValue;
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.categoryMaster = [];
      this.alertService.error(error);
    });
  }
  getTypeMaster(): any {
    this.spinner.show();
    this.typeService.getTypes().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.typeMaster = data.responseValue;
      }
    }, e => {
      this.spinner.hide();
      this.typeMaster = [];
    });
  }
  getExpenseLists(): any {
    this.spinner.show();
    this.expenseService.getExpenseMaster({id: '', userId: this.currentUser.id}).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.expenseLists = data.responseValue;
        this.dtTrigger.next('');
      }
    }, e => {
      this.spinner.hide();
      this.expenseLists = [];
      this.dtTrigger.next('');
      this.alertService.error(e.error.responseValue.message);
    });
  }
  addForm() {
    this.buttonLabel = 'Save Details';
    this.modalHeader = 'Add Expenses';
    $('#addModal').modal('show');
    this.form.reset();
    this.initForm();
  }
  initForm(): any {
    this.form = this.fb.group({
      id: [''],
      userId: [this.currentUser.id],
      type: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])],
      date: [moment().format('YYYY-MM-DD'), Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required])],
      categoryId: ['', Validators.compose([Validators.required])],
      paymentId: ['', Validators.compose([Validators.required])],
      remarks: [''],
      fileUrl: ['']
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  formSubmit(value: any) {
    this.spinner.show();
    this.expenseService.saveExpenseMaster(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        $('#addModal').modal('hide');
        this.alertService.success(data.responseValue.message);
        this.rerender();
        this.getExpenseLists();
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.alertService.error(error);
    });
  }
  processFile(e: any) {
    this.uploadProfilePicture(e.target.files[0]);
  }
  uploadProfilePicture(fileData: string | Blob) {
    const formData = new FormData();
    formData.append('file', fileData);
    formData.append('type', 'Expense');
    this.spinner.show();
    this.expenseService.expenseUpload(formData).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
      } else if (event.type === HttpEventType.Response) {
        this.spinner.hide();
        let response: any;
        response = event.body;
        this.form.patchValue({
          fileUrl: response.url
        });
      }
    });
  }

  changeType(e: any) {
    const type = e.target.value;
    this.spinner.show();
    this.expenseService.typeBasedCategory({type}).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        this.categoryMaster = data.responseValue;
      }
    }, e => {
      this.spinner.hide();
      let error = e.error.responseValue.message;
      this.categoryMaster = [];
      this.alertService.error(error);
    });
  }
}
