import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {FormBuilder, FormGroup, Validator, Validators} from "@angular/forms";
import {UserService} from "../../../_services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(private spinner: NgxSpinnerService, private fb: FormBuilder, private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(): any {
    this.form = this.fb.group({
      userName: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    })
  }

  formSubmit(value: any) {
    console.log(value);
    this.spinner.show();
    this.userService.login(value).subscribe(data => {
      if (data.responseType === 'S') {
        console.log(data.responseValue);
        this.spinner.hide();
        localStorage.setItem('currentUser', JSON.stringify(data.responseValue));
        this.router.navigate(['/admin/dashboard']);
      }
    }, err => {
      this.spinner.hide();
      alert(err.error.responseValue);
    });
  }
}
